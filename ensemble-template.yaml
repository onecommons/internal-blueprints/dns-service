apiVersion: unfurl/v1alpha1
kind: Ensemble
spec:
  deployment_blueprints:
    kubernetes:
      title: Kubernetes
      cloud: unfurl.relationships.ConnectsTo.K8sCluster
      description: Deploy into a Kubernetes cluster

  service_template:
    metadata:
      template_name: Unfurl Cloud DNS Service
    description: Service for managing user and project subdomains

    repositories:
      types:
        url: https://gitlab.com/onecommons/unfurl-types.git

    imports:
      - file: service-template.yaml
        repository: types

    node_types:
      UnfurlDNSService:
        derived_from: unfurl.nodes.APIServiceApp
        properties:
          dns_zone:
            title: Domain name
            description: Base domain to add namespace to
            type: string
          dns_zone_id:
            title: Hosted Zone ID
            description: Route53 zone ID for the above domain
            type: string
          access_key_id:
            title: AWS Access Key ID
            description: Account credentials for DNS service account
            type: string
          secret_access_key:
            title: AWS Secret Access Key
            description: Account credentials for DNS service account
            type: string
          domain_quota:
            title: Domain Quota
            description: Maximum number of domains allowed for each namespace
            type: string
          num_workers:
            title: Number of workers
            type: integer
            default: 8
          cname_dns_servers:
            title: CNAME DNS Servers
            description: Comma separated list of servers to check
            type: string
            required: false

    topology_template:
      root:
        node: the_app

      node_templates:
        the_app:
          type: UnfurlDNSService
          requirements:
            # ContainerApp defined two requirements
            # you can use the names of these requirements to access the instances they point to
            - container: container_service
            - api_definition: project-dns-api

        container_service:
          type: unfurl.nodes.ContainerService
          properties:
            container:
              ports:
                - "5000"
              command: [
                  "gunicorn",
                  "-b",
                  "0.0.0.0:5000",
                  "--log-level",
                  "debug",
                  "-w",
                  '"{{ ROOT.app.num_workers }}"', # workaround jinja2 native type conversion
                  "app:app",
                ]
              environment:
                DNS_ZONE: "{{ ROOT.app.dns_zone }}"
                DNS_ZONE_ID: "{{ ROOT.app.dns_zone_id }}"
                AWS_ACCESS_KEY_ID: "{{ ROOT.app.access_key_id }}"
                AWS_SECRET_ACCESS_KEY: "{{ ROOT.app.secret_access_key }}"
                DNS_RECORD_QUOTA: "{{ ROOT.app.domain_quota }}"
                CNAME_USE_NAMESERVERS: "{{ ROOT.app.cname_dns_servers }}"
          requirements:
            - container_image_source: local_image
            - host: dockerhost

        dockerhost:
          type: unfurl.nodes.K8sContainerHost

        local_image:
          type: UnfurlCloudMirroredRepoImageSource
          properties:
            registry_url:
              get_env: CI_REGISTRY
            username:
              get_env: GITLAB_USER_LOGIN
            password:
              get_env: UNFURL_ACCESS_TOKEN
            remote_git_url:
              get_env: BLUEPRINT_PROJECT_URL
            git_user:
              get_env: GITLAB_USER_LOGIN
            git_password:
              get_env: UNFURL_ACCESS_TOKEN
            branch: main
            project_id: internal/blueprints/dns-service
            repository_id: internal/blueprints/dns-service/main
            # project_id: "{{ lookup('env', 'BLUEPRINT_PROJECT_URL') |  regex_replace('.git$', '') | urlsplit('path') | trim('/') }}"
            # repository_id: "{{ lookup('env', 'BLUEPRINT_PROJECT_URL') |  regex_replace('.git$', '') | urlsplit('path') | trim('/') }}/main"

        project-dns-api:
          type: unfurl.nodes.UnfurlCloudAPIDefinition
          properties:
            name: project-dns
            api_root: "{{ ROOT.app.api_root }}"
            target_url: "http://{{ ROOT.container.host }}:{{ ROOT.container.port }}"
            auth_rules:
              auth_project_access: maintainer
              ttl: 3600       # cache auth for 1h
              user_rate:
                write: 10     # 10 updates
              user_per:
                write: 86400  # every day
              domain_quota: "{{ ROOT.app.domain_quota }}"
