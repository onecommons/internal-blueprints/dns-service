import os

import boto3
import dns.exception
import dns.resolver
from flask import Flask, request
import cname_verification

app = Flask(__name__)
app.register_blueprint(cname_verification.cname_verification, url_prefix='/cname_verification')

BASE_ZONE = os.getenv("DNS_ZONE")
ZONE_ID = os.getenv("DNS_ZONE_ID")
assert BASE_ZONE
assert ZONE_ID

# set to 0 to disable quota checking
MAX_RECORDS = int(os.getenv("DNS_RECORD_QUOTA") or 0)
assert MAX_RECORDS


@app.route("/health")
def health():
    return "OK"


@app.route("/update_zone/<subdomain>/<action>/<type>/<value>", methods=["POST"])
def update_zone(action: str, subdomain: str, type: str, value: str):
    namespace = request.args.get("auth_project", "").split("/")
    _project = namespace.pop()

    assert namespace and len(namespace) >= 0
    assert subdomain
    assert action in ["CREATE", "DELETE", "UPSERT"]
    assert type in [
        "SOA",
        "A",
        "TXT",
        "NS",
        "CNAME",
        "MX",
        "NAPTR",
        "PTR",
        "SRV",
        "SPF",
        "AAAA",
        "CAA",
        "DS",
    ]

    # make sure user is not overriding our special domain
    if "_unfurl" in subdomain.split("."):
        return {"message": {"invalid subdomain"}}, 400

    # use reverse-DNS for longer namespaces
    namespace_rdns = ".".join(reversed(namespace))
    zone = f"{namespace_rdns}.{BASE_ZONE}"

    # adjust recorded quota as needed
    quota = check_quota(zone)
    new_quota = max(quota - 1, 0) if action == "DELETE" else quota + 1

    # if max quota is set, prevent creation of new records (but allow deletion if over quota)
    if MAX_RECORDS > 0 and new_quota > MAX_RECORDS and action != "DELETE":
        return {"message": f"domain quota exceeded (max of {MAX_RECORDS})"}, 400

    # fully qualified domain name
    fqdn = f"{subdomain}.{zone}"

    print(f"running {action} on {fqdn} {type} {value} (zone {ZONE_ID})")
    # https://hands-on.cloud/working-with-route53-in-python-using-boto3/#h-create-a-resource-record
    r53 = boto3.client("route53")
    records = [
        # add user record
        {
            "Action": action,
            "ResourceRecordSet": {
                "Name": fqdn,
                "ResourceRecords": [
                    {
                        "Value": value,
                    },
                ],
                "TTL": 600,
                "Type": type,
            },
        },
        # update quota record
        {
            "Action": "UPSERT",
            "ResourceRecordSet": {
                "Name": f"_unfurl.{zone}",
                "ResourceRecords": [
                    {
                        "Value": f'"unfurl-quota={new_quota}"',
                    },
                ],
                # use minimum ttl of 1 for minimal record caching
                # TODO: this is a race condition! this should use something atomic instead of a cached record!
                # but should:tm: work for now since currently deployments are sequential on Gitlab
                "TTL": 1,
                "Type": "TXT",
            },
        },
    ]

    response = r53.change_resource_record_sets(
        ChangeBatch={
            "Changes": records,
            "Comment": "DNS Service",
        },
        HostedZoneId=ZONE_ID,
    )

    response["name"] = fqdn
    response["type"] = type
    response["value"] = value
    response["zone"] = zone
    # if response["HTTPStatusCode"] != 200 failed
    return response


# fetch current record usage from special TXT record on zone
def check_quota(zone):
    try:
        txt_records = dns.resolver.resolve(f"_unfurl.{zone}", "TXT")
        # txt records are quoted
        txt_values = [r.to_text().strip('"') for r in txt_records]
        # split txt key=value records into dict
        txt_hash = {k: v for k, _, v in [r.partition("=") for r in txt_values]}

        # return 0 if our record is missing...
        return int(txt_hash.get("unfurl-quota", 0))

    except dns.exception.DNSException:
        # ...or the dns query otherwise failed (e.g. no record defined)
        return 0


print(f"DNS service loaded for {BASE_ZONE}")
print(f"Current quota: {MAX_RECORDS}")
