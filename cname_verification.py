from flask import Blueprint, request, jsonify
import os
import re
import urllib.request
import urllib.error
import ssl
from dataclasses import dataclass
from typing import Dict, Optional, Union
import time
import sys
import traceback
import dns.resolver
import dns.rdatatype

DNS_NAMESERVERS = {
    'Google Public DNS': ['8.8.8.8', '8.8.4.4'],
    'Quad9': ['9.9.9.9', '149.112.112.112'],
    'OpenDNS': ['208.67.222.222', '208.67.220.220'],
    'Cloudflare': ['1.1.1.1', '1.0.0.1']
}

USE_NAMESERVERS = re.split(
    r'\s*,\s*',
    os.getenv('CNAME_USE_NAMESERVERS', 'GooglePublicDNS,Quad9,OpenDNS,Cloudflare')
)

for nameserver in list(DNS_NAMESERVERS.keys()):
    if not any(ns for ns in USE_NAMESERVERS if re.sub(r'\s+', '', nameserver).lower() == ns.lower()):
        del DNS_NAMESERVERS[nameserver]

EXPECTED_BACKEND_CONTENT = os.getenv('CNAME_EXPECTED_BACKEND_CONTENT', 'default backend - 404')

@dataclass
class CnamePair:
    host: str
    target: str

@dataclass
class VerificationResult:
    nameserver_results: Optional[Dict[str, bool]]
    target_reachable: bool

def verify_target_reachable(host: str) -> bool:
    def get_content():
        try:
            context = ssl.create_default_context()
            context.check_hostname = False
            context.verify_mode = ssl.CERT_NONE
            result = urllib.request.urlopen(f"https://{host}", context=context)
        except urllib.error.HTTPError as e:
            result = e

        return result.read().decode('utf-8')
        
    try:
        content = get_content()
    except:
        return False

    return content == EXPECTED_BACKEND_CONTENT

ANSWERS_CACHE: Dict[str, bool] = {}

def verify_cname_pair(pair: CnamePair, follow=False) -> VerificationResult:
    nameserver_results = dict()
    resolver = dns.resolver.Resolver(configure=False)
    reachable = verify_target_reachable(pair.host)

    # avoid lookup when possible
    if not reachable: return VerificationResult(None, reachable)

    for (nameserver_name, nameserver_ips) in DNS_NAMESERVERS.items():
        cache_key = ':'.join([nameserver_name, pair.host, pair.target, str(follow)])

        if ANSWERS_CACHE.get(cache_key, False):
            nameserver_results[nameserver_name] = True
            continue

        resolver.nameservers = nameserver_ips

        try:
            if follow:
                canonical_name = str(resolver.canonical_name(pair.host))
            else:
                try:
                    answer = resolver.resolve(pair.host, dns.rdatatype.CNAME)
                    canonical_name = str(answer[0].target)
                except IndexError:
                    canonical_name = None
                except:
                    traceback.print_exc(file=sys.stderr)
                    canonical_name = None

        # TODO ratelimit ourselves 
        except dns.resolver.LifetimeTimeout as e:
            raise e 

        result = canonical_name == pair.target or canonical_name == f"{pair.target}."
        if result:
            ANSWERS_CACHE[cache_key] = True
        nameserver_results[nameserver_name] = result

    return VerificationResult(nameserver_results, reachable)


def handle_verify_cname(host: str, target: Optional[str] = None) -> VerificationResult:
    if target is None:
        return VerificationResult(None, verify_target_reachable(host.lower()))
    else:
        return verify_cname_pair(CnamePair(host.lower(), target.lower()))

cname_verification = Blueprint('cname_verification', __name__)

@cname_verification.route('/<host>', methods=["GET"])
def verify_cname(host: str):
    target = request.args.get('target')
    try:
        assert host
    except:
        return "Invalid parameters", 400
    return jsonify(handle_verify_cname(host, target))

if __name__ == '__main__':
    import unittest

    print(f'{DNS_NAMESERVERS=}')
    print(f'{EXPECTED_BACKEND_CONTENT=}')

    class TestCnameVerification(unittest.TestCase):
        def test_target_reachability(self):
            host = 'foo.u.opencloudservices.net'
            self.assertTrue(handle_verify_cname(host).target_reachable)

        def test_cname_pair(self):
            host = "foo.u.opencloudservices.net"
            target = "cname-check.opencloudservices.net"

            result = handle_verify_cname(host, target)#verify_cname_pair(CnamePair(host, target))
            for nameserver_name, nameserver_status in result.nameserver_results.items():
                self.assertTrue(nameserver_status, f"{nameserver_status} should have the correct cname")

    unittest.main()

