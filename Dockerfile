FROM python:3.8-alpine

WORKDIR /data

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY app.py .
COPY cname_verification.py .

EXPOSE 5000
CMD ["gunicorn", "-b", "0.0.0.0:5000", "-w", "8", "app:app"]
